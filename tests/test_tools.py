"""tools tests module"""
import tekigo.tools as tools
import numpy as np

# unitary tests
def test_node_volume_approx():
    """approximation of node volume from cells volumes"""
    node_vol_tgt = 1.
    edge_tgt = 1.
    cell_tgt = 1.

    assert node_vol_tgt == tools.approx_vol_node_from_vol_cell(1. / 4.5)
    assert node_vol_tgt == tools.approx_vol_node_from_edge(np.power(4. * np.sqrt(2.) / 3., 1. / 3.))
    assert cell_tgt == tools.approx_vol_cell_from_vol_node(1. * 4.5)
    assert edge_tgt == tools.approx_edge_from_vol_node(3. / (4. * np.sqrt(2.)))
