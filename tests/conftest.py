import os
from distutils import dir_util
import h5py
import pytest
from arnica.utils import h5_same

@pytest.fixture(scope='module')
def datadir(tmpdir_factory, request):
    '''
    Fixture responsible for searching a folder with the same name as test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.
    '''
    file_path = request.module.__file__
    test_dir, _ = os.path.splitext(file_path)
    dir_name = os.path.basename(test_dir)

    datadir_ = tmpdir_factory.mktemp(dir_name)
    dir_util.copy_tree(test_dir, str(datadir_))

    return datadir_

#h5file comparison tool
@pytest.fixture(scope='module')
def h5same_files():
    return h5_same

@pytest.fixture(scope='module')
def is_word_in_file():

    def check_word_in_file(filename, search_string):
        """ Function to check if a word is present in a file.

        Input:
            :filename: str, path to file
            :search_string: str, word(s) to look for in file
        Output:
            :True or False boolean whether search_word has been found or not
        """
        #print(request.param)
        with open(filename, 'r') as fin:
            content = fin.read()
            if search_string in content:
                return True
            else:
                return False
    return check_word_in_file
