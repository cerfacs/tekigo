# Contributing to TEKIGO

## The source

If you have access to the sources, developments should be performed using git.
To start changing the code, please create a new branch labeled
`feature_{new_feature}` (*e.g*: `feature_my_model`). Please choose a clear name
to describe your feature, and make separate branches for separate features.
If the changes are to be included in a long term merge, add FUTURE/ in front of 
branch name

### Git commit messages

Git commit messages must meet some *quality standards*. You can find general
ideas *e.g.* [here](https://chris.beams.io/posts/git-commit/). The basic
concepts are listed as 7 rules:

 1. Separate subject from body with a blank line
 2. Limit the subject line to 50 characters
 3. Capitalize the subject line
 4. Do not end the subject line with a period
 5. Use the imperative mood in the subject line
 6. Wrap the body at 72 characters
 7. Use the body to explain what and why vs. how

Since HULK is hosted on a GitLab server, the
[markdown](https://en.wikipedia.org/wiki/Markdown) format is supported. Feel
free to use it to make your code more clear. GitLab also understands references
to issues (*e.g.* #5 will link to issue number 5 in project), merge requests
(!3) and more. For a full list, see the
[GitLab documentation](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/).
