#!/bin/bash

echo ">>> clean documentation"
rm -rf api
make clean

echo ">>> rebuild api"
sphinx-apidoc ../src/tekigo -o ./api --module-first  --no-toc

echo ">>> rebuild html"
make html

#echo ">>> move documentation inside sources"
#rm -rf ../src/pyavbp/dochtml
#cp -rf _build/html ../src/pyavbp/dochtml
