
## Metric filters

Tekigo also ships some functions that are often used when adapting a mesh.
These functions can be dangerous and must be called explicitly.

### Filter outliers

Behind this appealing name is hidden a function that **clips** your field where values are larger than 3 standard deviations away from the average. You can call it with:

```python
from tekigo.tools import filter_outliers
(...)
metric_field2 = filter_outliers(metric_field)
```

However the effect of this function is extremely case-dependent.
To ilustrate this, tekigo provides the `auto_hist` function, which plot an histogram of a value.
The syntax is simply:

```python
from tekigo.tools import auto_hist
(...)
auto_hist(metric_field)
```

Assume a bimodal distribution, with a lot of values near 300, and a smaller amount around 1200.
If the first mode is sufficiently small compared to the second, only "real" outliers will be clipped.

![Tubular exchanger](../_images/metricfilters/filterin.png)

*On this field, `filter_outliers()` could remove some rare outliers*

... but if the first mode is too large (more values, *i.e.* finer mesh there), the standard deviation shrinks, and the second mode can be cropped:

![Tubular exchanger](../_images/metricfilters/filterout.png)

*On this field, `filter_outliers()` will remove 60% of the second peak*


### Normalize metric

`normalize_metric(values, expnt=10.0, floor=0.3)` is a typical normalization function:

1. remove outliers.
2. rescale to the [0, 1] support.
3. morph the values with a power law with an exponent `expnt`.
4. rescale to the [`floor`, 1] support.

However, this normalization is **not mandatory**, and is not suited to many situations.
In particular, the first test calls `filter_outliers()`  which can destroy your metric for the reasons given before.

Call it with:

```python
from tekigo.tools import normalize_metric
(...)
metric_field = normalize_metric(qoi)
```
