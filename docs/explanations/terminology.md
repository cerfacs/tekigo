## Tekigo terminology

**The metric**

As the meshing is ultimately performed by MMG, the inputs need to be tailored to its needs. A "future edge size"  field is what MMG bases its refinement on. HIP relies instead on a parameter called the **metric** which is more intuitive than the latter field and internally generates the "correct" input for MMG.  Similar to any other field variable, the **metric** (and also the future edge size ) has a value assigned to each mesh node. Requirements are that
* the **metric** values have to be ∈  [0.5, 4] with 
    * &lt; 1:   refine
    * = 1:      do nothing
    * &gt;  1:  coarsen
* and where 
    * 0.5:  implies that the target edge length will be half of the current one
    * 4:    implies that the target edge length will be 4 times larger that the current one

MMG <ins>cannot guarantee </ins> that nodes with a metric value of 1 assigned to them will not be affected by the refinement, as multiple parameters come into play. The same comment is valid for other values. <ins>The refinement procedure tries to comply as much as possible with the metric field values.  </ins>

**The criterion**

Tekigo also works with the concept of a **criterion** (or criteria), and is relevant to the `indirect` adaptation introduced further below. Similarly to the metric, it is a field variable with a value assigned to each node. 
* A **criterion** has to be ∈  [-1, - 1] with 
    * 1:    maximum refinement 
    * 0:    do nothing
    * -1:  maximum coarsening

Through the intermediary of the `calibrate_metric()` function, <ins>a **criterion** is converted into a metric</ins>, complying with the previously mentioned requirements, as to enable the use of MMG.

**A criterion can best be  understood as being a mask, where one specifies whether one wishes to refine, coarsen or do nothing. The amount of refinement is then controlled by the mixing of the criteria.**