## Moving to version 1.0

Since April 2020, Tekigo moved to ver 1.0. This is a **major** release meaning former scripts will not work anymore. Here is what you need to knowx to adapt your scripts:



### "Raw adapt" becomes  direct adaptation


The 0.X simple script for a direct adaptation where the metric is quantitatively enforced locally at all places  was looking like this

```python
import numpy as np
from tekigo import (TekigoSolution,
                    raw_adapt)

tekigo_sol = TekigoSolution(mesh='default.mesh.h5',
                            solution='default_inst.sol.h5',
                            overwrite_dir=True,
                            only_sol_vars=['temperature'],
                            out_dir='./Results')

temp = tekigo_sol.load_current_solution()['temperature']

crit = np.array((temp - 800) / (1500 - 800))
crit = np.clip(met, 0, 1)
metric_field =  0.8  + (1.-crit)*0.4

raw_adapt(tekigo_sol, metric_field)
```

Since version 1.0, the same script is expressed with:


```python
import numpy as np
from tekigo import (TekigoSolution, adaptation_pyhip)

tekigo_sol = TekigoSolution(
    mesh='default.mesh.h5',
    solution='default_inst.sol.h5',
    out_dir='./Results')

x_coor = tekigo_sol.load_qoi('/Additionals/temperature')
crit = np.array((temp - 800) / (1500 - 800))
crit = np.clip(met, 0, 1)
metric_field =  0.8  + (1.-crit)*0.4

tekigo_sol.evaluate_metric(metric_field)

# adaptation_pyhip(tekigo_sol)
```

The former `dry_run=true` option of `raw_adapt()` is done by uncommenting the call to  `adaptation_pyhip()`.




### "Refine" becomes  indirect adaptation


The 0.X simple script for a direct adaptation where the metric is qualitatively enforced locally at all places , but the qunatity of adaptation was given globally with a nuber of cells, was looking like this


```python
import numpy as np
from tekigo import (TekigoSolution,
                    refine)

def custom_criteria(tkg_sol):
    """Funtion to wrap the criterions


    This criteria will be called at each new adaptation mesh.
    Therefore the size of the array, and the mesh properties,
    will change acordingly.

    It is YOUR duty to make a criteria that converges (or not)

    Parameter :
    -----------
    tkg_sol : an instance of tekigo solution.

    Returns :
    ---------
        a flat dictionary of np_arrays of shape (nnode)
        each entry is a criterion
    """

    mesh = tkg_sol.load_current_mesh()
    x_coor = mesh['/Coordinates/x']
     vol_node = mesh['/VertexData/volume']
    
    sol = tkg_sol.load_current_solution()
    like = sol['LIKE']
    yplus = sol['wall_Yplus']

    criteria = dict()  # This dict store all the criterias to use
    crit = (np.log10(like) - (-1))/((3) - (-1))
    crit = np.clip(crit, 0, 1.)
    # nullify criterion on the last half of the domain
    # Mash in MILLIMETERS, you bet!
    crit = np.where(x_coor > -180., crit, 0.)
    criteria['like'] =  crit

    # max if yplus > 1
    # log10 evolution
    crit = (yplus - (0))/(1 - 0)
    crit = np.clip(crit, 0, 1.)
    # nullify criterion on the last half of the domain
    # Mash in MILLIMETERS, you bet!
    crit = np.where(x_coor > -180., crit, 0.)
    criteria['yplus'] =  crit

    return criteria


def main():
    """Example of usage """
    tekigo_sol = TekigoSolution(mesh='default.mesh.h5',
                                solution='default_inst.sol.h5',
                                average_sol='average.h5',
                                out_dir='./Results',
                                overwrite_dir= True,
                                only_sol_vars=['temperature'],
                                only_msh_vars=['volume','/Coordinates/x'])

    refine(
        tekigo_sol,
        custom_criteria,
        dry_run=True,
        iteration_max=1,
        nnode_max=120000,
        min_edge=0.7e-3,
        l2_crit=0.005,
        coarsen=False)

if __name__ == "__main__":
    main()
```

In 1.X version, it looks like:

```python
import numpy as np
from tekigo import (TekigoSolution, calibrate_metric, adaptation_pyhip)

tekigo_sol = TekigoSolution(
    mesh='../../GILGAMESH/trapvtx/trappedvtx.mesh.h5',
    solution='default_inst.sol.h5',
    out_dir='./Results')

tekigo_sol.add_fields_from_solution(
    "average.h5", 
    "Average", 
    ["LIKE", "yplus"])

x_coor = tekigo_sol.load_qoi('/Mesh/coord_x')
vol_node = tekigo_sol.load_qoi('/Mesh/volume')
like = tekigo_sol.load_qoi('/Average/LIKE')
yplus = tekigo_sol.load_qoi('/Average/yplus')

criteria = dict()  # This dict store all the criterias to use
crit = (np.log10(like) - (-1))/((3) - (-1))
crit = np.clip(crit, 0, 1.)
# nullify criterion on the last half of the domain
# Mash in MILLIMETERS, you bet!
crit = np.where(x_coor > -180., crit, 0.)
criteria['like'] =  crit

# max if yplus > 1
# log10 evolution
crit = (yplus - (0))/(1 - 0)
crit = np.clip(crit, 0, 1.)
# nullify criterion on the last half of the domain
# Mash in MILLIMETERS, you bet!
crit = np.where(x_coor > -180., crit, 0.)
criteria['yplus'] =  crit

metric_field = calibrate_metric(
    tekigo_sol, 
    criteria, 
    target_ncells=400000, 
    edge_min=0.7e-3,
    
)
tekigo_sol.evaluate_metric(metric_field)
adaptation_pyhip(tekigo_sol)

```