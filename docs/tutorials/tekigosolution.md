
## Unsderstanding the tekigoSolution object

### What is it?

A tekigoSolution object is an unstructured mesh, plus fields on the mesh.
This object is **an interface to an actual set of three files** stored on your hard drive.
Theses files are usually:

```bash
Results/
├── tekigo_init.mesh.h5
├── tekigo_init.sol.h5
└── tekigo_init.sol.xmf
```

Therefore, if you add a field `new_field` in a python *interactive* script using tekigoSolution,
the files on the disc `.sol.h5` and  `.sol.xmf` are *immediately* updated. Reloading the solution from your fieldviewer (Paraview, Ensight, Tecplot) will make this `new_field` appear.

### Creation

Create your object as follows:

```python
from tekigo import TekigoSolution

tekigo_sol = TekigoSolution(
    mesh='../../GILGAMESH/trapvtx/trappedvtx.mesh.h5',
    solution='combu.init.h5',
    out_dir='./Results')
```

In the log file, and the standard input, the object will react like this:

```bash
======= Creating TekigoSolution handler... =========


   Inital mesh:
   Number of nodes     34684
   Number of cells     177563
   Minimal edge size   1.08462e-03

 The following Quantities of Interest can be loaded: 
 - /Additionals/pressure
 - /Additionals/temperature
 - /GaseousPhase/rho
 - /GaseousPhase/rhoE
 - /GaseousPhase/rhou
 - /GaseousPhase/rhov
 - /GaseousPhase/rhow
 - /Mesh/coord_x
 - /Mesh/coord_y
 - /Mesh/coord_z
 - /Mesh/init_edge
 - /Mesh/volume
 - /RhoSpecies/CO
 - /RhoSpecies/CO2
 - /RhoSpecies/H2O
 - /RhoSpecies/KERO_LUCHE
 - /RhoSpecies/N2
 - /RhoSpecies/O2
```

### The role of the initial solution?

The initial solution is highly recommended, because at the end of the day you want to run a simulation.
If you provide the solution at the very end of the process, the new mesh, old mesh and solution must be loaded in memory for an interpolation. This step is often very heavy for adapted large meshes.

One can easily skip this unecessary loading time by providing the solution at the debinning of the process.
Indeed, the mesh adaptation require new mesh, old mesh  and at least a metric. Most of the I/O is eventually done in this first step anyway.

But if you reallllly do not want to provide a solution, set it to, `None`.

### Skip additionals

The additionals fields can make the solution very heavy.
You can skip them with: `TekigoSolution(..., additionals=False)`.

### Loading a field

You can load any field by name into the tekigo solution as follows.

```python
x_coor = tekigo_sol.load_qoi('/Mesh/coord_x')
```

The resulting output is a numpy array with the shape of the mesh.

### Adding a field from a numpy array

You can add a field anytime in the object:

```python
 tekigo_sol.add_field_to_solmesh("foo", foo_array)
```

This is adding the field `/Adapt/foo` in the solution.

The group destination is by default `Adapt` but can be overidden, i.e. for `/Custom/foo` use:

```python
 tekigo_sol.add_field_to_solmesh("foo", foo_array, group='Custom")
```

### Adding a field from an HDF5 solution

Fields can be loaded from an existing solution file with:

```python
tekigo_sol.add_fields_from_solution('combu.init.h5', "RhoSpecies", ["CO","CO2"])
```

Note this works for any number of solutions , instantaneous or averages. 
You can therefore mix fields coming from various files with this.


### Setting the metric for adaptation

As the only objective of tekigo is mesh adaptation, the method `.evaluate_metric()` is adding two fields:

1. `/Adapt/target_edge` is the estimated  edge size after adaptation. This quantity is used for example as the input for the parallel adpation tool **TreeAdapt**.
1. `/Adapt/metric` is the ratio of the edge final size `/Adapt/target_edge`, over the initial size `/Mesh/init_edge`. This quantity is used for example as the input for the parallel adpation tool **pyHip**.

When called this method also gives some global information on the current adpatation metric:

```bash
======= Evaluate metric w.r. to solution =========


   Field Custom metric : 0.5000, 1.0000

   Forecast mesh:
   Number of nodes     36315
   Number of cells     185912
   Minimal edge size   9.44140e-04
```

### The gather scatter method()

The `.gatherscatter()` method returns an object to apply some advanced connectivity-based operations (closure, smoothing). Read more in the how-tos. 


