
## simple uses of tekigo

### A direct adaptation

The simplest adaptation is controlled by the following script:

```python
import numpy as np
from tekigo import (TekigoSolution, adaptation_pyhip)

tekigo_sol = TekigoSolution(
    mesh='../../GILGAMESH/trapvtx/trappedvtx.mesh.h5',
    solution='combu.init.h5',
    out_dir='./Results')

x_coor = tekigo_sol.load_qoi('/Mesh/coord_x')
metric_field = np.where(x_coor<0.1,1,0.7)

tekigo_sol.evaluate_metric(metric_field)

adaptation_pyhip(tekigo_sol, edge_low=1.e-3)
```

See the tutorial on `TekigoSolution()` for a complet introduction.
Once it is created, you can get a quantity of interest as a numpy array with `.qoi_load()`.
Nympy operations allows any combinations between the quantities of interest.
For a direct adaptation, the metric will be in ]0,1] for refinement, and >1 for coarsening.
 
The method `.evaluate_metric()` gives an estimation of the potential size of the adapted mesh, and stores the `metric` and the `target_edge` in the solution. It can be read from the file `tekigo_init.sol.h5`.

You can then use the function `adaptation_pyhip()` to use the built-in sequential mesh adaptation.

### An indirect adaptation

An indirect adaptation means that, instead of one metric, the user can built several criterias and pass them to `calibrate_metric()`. This function will :

- mix the criterias
- transform into a metric
- rescale the metric to ensure a global number of nodes
- clip locally the metric if the future edge is too small

The usage is:

```python
import numpy as np
from tekigo import (TekigoSolution, calibrate_metric, adaptation_pyhip)

tekigo_sol = TekigoSolution(
    mesh='../../GILGAMESH/trapvtx/trappedvtx.mesh.h5',
    solution='combu.init.h5',
    out_dir='./Results')

x_coor = tekigo_sol.load_qoi('/Mesh/coord_x')

criteria = {
       "crt_1" :  np.where(x_coor<0.1,1,0.0),
       "crt_2" :  np.where(x_coor>0.15,-1,0.0)
}
metric_field = calibrate_metric(
    tekigo_sol, 
    criteria, 
    target_ncells=400000, 
    edge_min=1.e-3,
    met_mix="abs_max",
)

tekigo_sol.evaluate_metric(metric_field)
adaptation_pyhip(tekigo_sol, edge_low=1.e-3)
```