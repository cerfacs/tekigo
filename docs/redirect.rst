


Tutorials
=========

These are the fastest guides to learn about this package.
See how you can use it without think too much about it.

.. toctree::
    :maxdepth: 2

    ./tutorials/tekigosolution
    ./tutorials/directindirectadapt
    


How-to Guides
=============

These are the step-by-step guides to see a practical usage of this package.
See what you can get of it in a nominal situation

.. toctree::
    :maxdepth: 2

    ./howto/metricfilters
    ./howto/gatherscatter
    ./howto/antares
    
References
==========

This the exhaustive api description ressource.
Dive there to leran how to use one the function of the package.

.. toctree::
    :maxdepth: 2

    ./api/tekigo

Explanations
============

This is where you will find some in-depth explanation.
You you question starts with "why ?", the answer is probaly here

.. toctree::
    :maxdepth: 2

    ./explanations/terminology
    ./explanations/moving_to_ver1
    